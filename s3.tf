provider "aws" {
  region  = "us-east-1"
  profile = "default"
}


resource "aws_launch_template" "my_template" {
  name_prefix   = "my_template"
  image_id      = "ami-0b0af3577fe5e3532"
  instance_type = "t2.micro"
}

resource "aws_autoscaling_group" "bar" {
  availability_zones = ["us-east-1a"]
  desired_capacity   = 5
  max_size           = 5
  min_size           = 1

  launch_template {
    id      = aws_launch_template.my_template.id
    version = "$Latest"
  }
}

resource "aws_s3_bucket" "oluse-bucket-2340722" {
  bucket = "oluse-bucket-234070"
  acl    = "private"

  tags = {
    Name = "My-bucket234070"
  }
}

variable "aws_region" {
  description = "this is your default aws region"
  default     = "us-east-1"
}
variable "public_cidr_blocks" {
  description = "these are public cidr blocks for your subnets"
  default     = ["172.51.1.0/24", "172.51.2.0/24", "172.51.3.0/24"]
}
variable "private_cidr_blocks" {
  description = "these are private cidr blocks for your subnets"
  default     = ["172.51.51.0/24", "172.51.52.0/24", "172.51.53.0/24"]
}

variable "availability_zones" {
  description = "these are availability zones for your subnets"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
  }

variable "env_name" {
  description = "this is the name of your environment"
  default     = "Dev"
}

variable "vpc_cidr_block" {
    default   = "172.51.0.0/16"
}

variable "my_instance_type" {
  description = "this is your default instance type"
  default     = "t2.medium"
}

variable "public_key_location" {
  description = "this is your public key location on your server"
  default     = "/home/user/.ssh/id_rsa.pub"
